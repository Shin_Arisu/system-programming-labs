#!/bin/sh

printf "Package manager wrapper\n\nThis program can display info about installed packages and install new packages.\n\nDeveloper: Vladimir Belkevich\n\n"
printf "You should synchronize with repository to fetch info \nabout packages and install new packages.\n\nDo you wish to synchronize? (y/n) \n"
read -r YN
while [ "$YN" != "y" ] && [ "$YN" != "n" ]; do
	printf "input 'y' or 'n' \n"
	read -r YN
done
if [ "$YN" = 'y' ]; then
	if ! dnf makecache 2>/dev/null; then
		printf 'err: Problem with dnf makecache\n' >&2
	fi
fi

STOPFLAG=0

while [ $STOPFLAG -eq 0 ]; do
	printf 'Enter package name: '
	read -r PKGNAME
	if dnf --cacheonly list installed "$PKGNAME" >/dev/null 2>&1; then
		if ! dnf --cacheonly info "$PKGNAME" 2>/dev/null; then
			printf '\npackage installed, but no info in cached database\n\n' >&2
		fi
	else
		if dnf --cacheonly list "$PKGNAME" >/dev/null 2>&1; then
			printf 'Package is not installed, do you wish to install %s? (y/n) \n' "$PKGNAME"
			read -r YN
			while [ "$YN" != "y" ] && [ "$YN" != "n" ]; do
				printf "input 'y' or 'n' \n Do you wish to install %s? y/n \n" "$PKGNAME"
				read -r YN
			done
			if [ "$YN" = 'y' ]; then
				dnf install "$PKGNAME"
			fi
		else
			printf 'Package doesnt exist\n\n'
		fi
	fi
	
	printf "Check another package? y/n \n"
	read -r YN
	while [ "$YN" != "y" ] && [ "$YN" != "n" ]; do
		printf "input 'y' or 'n' \n Check another package? y/n \n"
		read -r YN
	done
	if [ "$YN" = 'n' ]; then
		STOPFLAG=1
	fi
done

printf "exit...\n"
